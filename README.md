# Convolutional Neural Network
### Implemented Convolutional Neural Network From scratch.


In machine learning, a convolutional neural network (CNN, or ConvNet) is a class of deep, feed-forward artificial neural networks that has successfully been applied to analyzing visual imagery.

CNNs use a variation of multilayer perceptrons designed to require minimal preprocessing. They are also known as shift invariant or space invariant artificial neural networks (SIANN), based on their shared-weights architecture and translation invariance characteristics.

CNNs use relatively little pre-processing compared to other image classification algorithms. This means that the network learns the filters that in traditional algorithms were hand-engineered. This independence from prior knowledge and human effort in feature design is a major advantage.

They have applications in image and video recognition, recommender systems and natural language processing.


* Dataset : 42,000 Images of Hand Written Digtis (0-9) from MNIST database.

* Layers :
    1. 2 Convolution Layers
    2. 2 Fully Connected Layers
* Filter Size :
    1. 3x3x1x4  Filters used in Conv Layer 1
    2. 3x3x4x16 Filters used in Conv Layee 2
* Actvation Function :
    1. Sigmoid
    2. Softmax
* Data Distribution :
    1. Train Set = 30,000 Images
    2. Cross Valid Set = 6000 Images
    3. Test Set = 6000 Images
* Libraries : 
    1. Numpy
    2. Pandas
    3. Matplot
* Results :
    1. Train Accuracy : 87%
    2. Valid Accuracy : 87%
    3. Test Accuracy  : 87%